package no.uib.inf101.sem2.grid;

public record GridCell<E>(CellPosition pos, E value) {
    public CellPosition getPosition() {
        return pos;
    }
    
    public E getContent() {
        return value;
    }

    
}

