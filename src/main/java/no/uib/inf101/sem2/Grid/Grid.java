package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;;
public class Grid<E> implements IGrid<E> {
    //feltvariabel for å lagre verdiene
    private List<List<E>> grid;
    //konstruktør 1
    public Grid(int rows, int cols){
        this(rows, cols, null);
    }
    // konstruktør 2
    public Grid(int i, int j, E defaultValue) {
        this.grid = new ArrayList<>();
        for (int row = 0; row<i; row++){
            ArrayList<E> theRow = new ArrayList<E>();
        for(int c = 0; c < j; c++){
            theRow.add(defaultValue);
        }
        this.grid.add(theRow);
    }
    }
    @Override
    public void set(CellPosition pos, E value){
        this.grid.get(pos.row()).set(pos.col(), value);
    }
    
    @Override
    public int rows() {
        return this.grid.size();
    }
    @Override
    public int cols() {
        return this.grid.get(0).size();
    }
    @Override
    public Iterator<GridCell<E>> iterator() {
        //var en metode i lab 4, sjekk videonotat
        List<GridCell<E>> Cells = new ArrayList<>();
            for(int i = 0; i<this.rows(); i++) {
    
                for(int j = 0; j<this.cols(); j++) {
    
                E value = grid.get(i).get(j);
    
                CellPosition newCellPosition = new CellPosition(i, j);
    
                GridCell<E> newGridCell = new GridCell<>(newCellPosition, value);
    
                Cells.add(newGridCell);
                }
            }
    
            return Cells.iterator();
        }
    
    
    @Override
    public E get(CellPosition pos) {
        int row = pos.row();
        int col = pos.col();
        return grid.get(row).get(col);
    }
    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        //returnere grid på 4x7 og pos (3,6) = true, 
        // (7, 8), vil være false
        if (pos.row() < 0) {
            return false;
        } if (pos.col() < 0) {
            return false;
        } if (pos.row() >= this.rows()) {
            return false;
        } if (pos.col() >= this.cols()) {
            return false;
        } else {
            return true;
        }
    }
}
