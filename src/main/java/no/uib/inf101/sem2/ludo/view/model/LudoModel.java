package no.uib.inf101.sem2.ludo.view.model;


import java.util.Random;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.view.ViewableLudoModel;
import java.util.Random;
//import no.uib.inf101.tetris.controller.ControllableTetrisModel;


public class LudoModel implements ViewableLudoModel{
    public LudoBoard board;
    private static final int min = 1;
    private static final int max = 6;
    
    

    public LudoModel(LudoBoard board){
        this.board = board;
        
    }


    @Override
    public GridDimension getDimension() {

        return this.board;

    }

    

    public boolean isPositionValid() {
        return true;
    }


    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        
        return this.board;
    }


    @Override
    public int Dice() {
        int random_int = (int)Math.floor(Math.random()* (max-min + 1) + min);
        return random_int;
    }
        


} 
    

