package no.uib.inf101.sem2.ludo.view.model;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import no.uib.inf101.sem2.view.LudoView;

public class Controller implements KeyListener {
  private final LudoModel model;
  private final Component view;

  public Controller(LudoModel model, Component view) {
    this.model = model;
    this.view = view;

    // Capture keyboard input when the view is in focus and send it here
    this.view.setFocusable(true);
    this.view.addKeyListener(this);
  }

  @Override
  public void keyPressed(KeyEvent e) {
    this.model.Dice();

    //if (e.getKeyCode() == KeyEvent.VK_LEFT) {
        // Left arrow was pressed
        //model.movePlayer(0,-1);
    //}
    //else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
        // Right arrow was pressed
        //model.movePlayer(0,1);
    //}
    //else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
        // Down arrow was pressed
        //model.movePlayer(1,0);
    //}
    //else if (e.getKeyCode() == KeyEvent.VK_UP) {
        // Down arrow was pressed
        //model.moveplayer(-1,0);
    //}

    this.view.repaint();
  }

  @Override
  public void keyTyped(KeyEvent e) {
    // ignore
  }

  @Override
  public void keyReleased(KeyEvent e) {
    // ignore
  }
}
