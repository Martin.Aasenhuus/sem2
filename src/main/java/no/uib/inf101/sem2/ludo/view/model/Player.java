package no.uib.inf101.sem2.ludo.view.model;

import java.awt.Color;
import java.util.Iterator;

//import apple.laf.JRSUIConstants.Size;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

public final class Player implements Iterable<GridCell<Character>>{
    private char colorname;
    private CellPosition cellPosition;
    private int currentPosition;
    private Object position;
    

    private Player(char colorname, CellPosition cellPosition){
        this.colorname = colorname;
        this.cellPosition = cellPosition;
        

    }

    //public static Player newPlayer(char color) {
        //switch(color) {
           //case 'B':
               //return new Player(color, {
           //}, new CellPosition(0, 0));
        //}


    public Player(String name, Color color){
        //this.name = name;
        //this.color = color;
        this.currentPosition = -1;

    }

    public void move(int steps) {
        currentPosition += steps;
        // legg til logikk for å håndtere når en brikke passerer startposisjonen
    }

    
    public boolean isInHomeArea() {
        return currentPosition < 0;
    }

    @Override
    public Iterator<GridCell<Character>> iterator() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'iterator'");
    }

     
}
