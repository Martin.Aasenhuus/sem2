package no.uib.inf101.sem2.ludo.view.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

public class LudoBoard extends Grid<Character>{


        public LudoBoard(int rows, int cols){
            super(rows, cols, '-');

            int[][] greenHomePos = { { 2, 2 }, { 2, 3 }, { 3, 2 }, { 3, 3 } };
	        
	        
	        int[][] blueHomePos = { { 11, 11 }, { 11, 12 }, { 12, 11 }, { 12, 12 } };

            int[][] greenPath = { { 6, 1 }, { 6, 2 }, { 6, 3 }, { 6, 4 }, { 6, 5 }, 
			{ 5, 6 }, { 4, 6 }, { 3, 6 }, { 2, 6 }, { 1, 6 }, { 0, 6 }, 
			{ 0, 7 }, { 0, 8 }, { 2, 8 }, { 3, 8 }, { 4, 8 }, { 5, 8 }, 
			{ 6, 9 }, { 6, 10 }, { 6, 11 }, { 6, 12 }, { 6, 13 }, { 6, 14 }, 
			{ 7, 14 }, { 8, 14 }, { 8, 12 }, { 8, 11 }, { 8, 10 }, { 8, 9 }, 
			{ 9, 8 }, { 10, 8 }, { 11, 8 }, { 12, 8 }, { 13, 8 }, { 14, 8 }, 
			{ 14, 7}, { 14, 6 }, { 12, 6 }, { 11, 6 }, { 10, 6 }, { 9, 6 }, 
			{ 8, 5 }, { 8, 4 }, { 8, 3 }, { 8, 2 }, { 8, 1 }, { 8, 0 }, 
			{ 7, 0 }, { 7, 1 }, { 7, 2 }, { 7, 3 }, { 7, 4 }, { 7, 5 }, { 7, 6 } };

            int[][] bluePath = { { 8, 13 }, { 8, 12 }, { 8, 11 }, { 8, 10 }, { 8, 9 }, 
			{ 9, 8 }, { 10, 8 }, { 11, 8 }, { 12, 8 }, { 13, 8 }, { 14, 8 }, 
			{ 14, 7}, { 14, 6 }, { 12, 6 }, { 11, 6 }, { 10, 6 }, { 9, 6 }, 
			{ 8, 5 }, { 8, 4 }, { 8, 3 }, { 8, 2 }, { 8, 1 }, { 8, 0 }, 
			{ 7, 0 }, { 6, 0 }, { 6, 2 }, { 6, 3 }, { 6, 4 }, { 6, 5 }, 
			{ 5, 6 }, { 4, 6 }, { 3, 6 }, { 2, 6 }, { 1, 6 }, { 0, 6 }, 
			{ 0, 7 }, { 0, 8 }, { 2, 8 }, { 3, 8 }, { 4, 8 }, { 5, 8 }, 
			{ 6, 9 }, { 6, 10 }, { 6, 11 }, { 6, 12 }, { 6, 13 }, { 6, 14 }, 
			{ 7, 14 }, { 7, 13 }, { 7, 12 }, { 7, 11 }, { 7, 10 }, { 7, 9 }, { 7, 8 } };
        }
    
    
        public String prettyString() {
            String sb = "";
            for (int row = 0; row < rows(); row++) {
              for (int col = 0; col < cols(); col++) {
                CellPosition position = new CellPosition(row, col);
                Character value = get(position);
                sb += value;
              }
            sb += System.lineSeparator();
            }
            return sb.trim();
          }


} 

