package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.List;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.awt.Font;

import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class LudoView extends JPanel {

    
        private ViewableLudoModel model;
        private ColorTheme colorTheme;
        private CellPositionToPixelConverter converter;

    
        public LudoView(ViewableLudoModel model) {
            this.model = model;
            this.colorTheme = new DefaultColorTheme();
            Color backgroundColor = colorTheme.getBackgroundColor();
            this.setBackground(backgroundColor);
    
            // Calculate preferred size based on the size of the game board
            GridDimension gd = model.getDimension();
            double margin = 5;
            double cellSize = 35;
            double boardWidth = gd.cols() * cellSize + margin * (gd.cols() + 1);
            double boardHeight = gd.rows() * cellSize + margin * (gd.rows() + 1);
            double frameWidth = 50;
            double totalWidth = boardWidth + frameWidth * 2;
            double totalHeight = boardHeight + frameWidth * 2;
            Dimension preferredSize = new Dimension((int) totalWidth, (int) totalHeight);
            this.setPreferredSize(preferredSize);
    
            // Create pixel converter for cell positions
            Rectangle2D box = new Rectangle2D.Double(frameWidth, frameWidth, boardWidth, boardHeight);
            this.converter = new CellPositionToPixelConverter(box, gd, margin);
        }
    
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            drawGame(g2);
            Font currentFont = g.getFont();
            Font newFont = currentFont.deriveFont(currentFont.getSize() * 1.6F);
            g.setFont(newFont);
            g.setColor(Color.black);
            g.drawString("" + this.model.Dice(), this.getWidth()/2, 25);
            g.drawString("Grønn:", this.getWidth()/3, 25);
        }
    
        public void drawGame(Graphics2D g2){
            // Draw frame around the board
            double margin = 5;
            double frameWidth = 50;
            double cellSize = 35;
            double boardWidth = model.getDimension().cols() * cellSize + margin * (model.getDimension().cols() + 1);
            double boardHeight = model.getDimension().rows() * cellSize + margin * (model.getDimension().rows() + 1);
            Rectangle2D box = new Rectangle2D.Double(frameWidth, frameWidth, boardWidth, boardHeight);
    
            g2.setColor(colorTheme.getFrameColor());
            g2.fill(box);

            //g2.fillOval(100, 150, 100, 100);
    
            // Draw cells on the board
            Iterable<GridCell<Character>> cells = this.model.getTilesOnBoard();
            
            drawCells(g2, cells, colorTheme);
            //drawCells(g2, this.model.PlayersFactory(), cells, colorTheme);
            //g2.fillOval(100, 150, 100, 100);
        }
    
        private void drawCells(Graphics2D g2, Iterable<GridCell<Character>> cells, ColorTheme colorTheme) {
            for (GridCell<Character> cell : cells) {
                Rectangle2D bounds = converter.getBoundsForCell(cell.pos());
                Color color = colorTheme.getCellColor(cell.value());
                g2.setColor(color);
                g2.fill(bounds);
            }
        } 
    
        @Override
        public Dimension getPreferredSize() {
            return super.getPreferredSize();
        }
    }


