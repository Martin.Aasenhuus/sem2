package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {

    Rectangle2D box;

    GridDimension gd;

    double margin;

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D getBoundsForCell(CellPosition cp){
        double cellWidth = (box.getWidth() - (margin * (gd.cols() + 1 ))) / gd.cols();
        double cellHeight = (box.getHeight() - (margin * (gd.rows() + 1 ))) / gd.rows();
        double x = box.getX() + margin + (cp.col() *  (cellWidth + margin));
        double y = box.getY() + margin + (cp.row() *  (cellHeight + margin));
        return new Rectangle2D.Double(x, y, cellWidth, cellHeight);
    }   

    
}
