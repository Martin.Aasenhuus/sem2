package no.uib.inf101.sem2.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme{

    @Override
    public Color getCellColor(char c) {
        Color color = switch(c) {
            case 'r' -> Color.RED;
            case 'g' -> Color.GREEN;
            case 'y' -> Color.BLUE;
            case 'b' -> Color.YELLOW;
            case 'p' -> Color.PINK;
            
            
            // .... fyll ut dine favorittfarger
            case '-' -> Color.WHITE;
            default -> throw new IllegalArgumentException(
                "No available color for '" + c + "'");
          };
          return color;
        
    }

    @Override
    public Color getFrameColor() {
        Color color = Color.lightGray;
        return color;
        
    }

    @Override
    public Color getBackgroundColor() {

        return null;
    }

    
    

 
    
}
