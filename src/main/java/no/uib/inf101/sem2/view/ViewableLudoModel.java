package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;


public interface ViewableLudoModel {
    GridDimension getDimension();

    Iterable<GridCell<Character>> getTilesOnBoard();

    int Dice();
}
