package no.uib.inf101.sem2;

import javax.swing.JFrame;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.ludo.view.model.Controller;
import no.uib.inf101.sem2.ludo.view.model.LudoBoard;
import no.uib.inf101.sem2.ludo.view.model.LudoModel;
import no.uib.inf101.sem2.view.LudoView;
import no.uib.inf101.sem2.view.SampleView;
import no.uib.inf101.sem2.view.ViewableLudoModel;

public class Main {

  public static final String WINDOW_TITLE = "Semesteroppgave 2 : Ludo";
  
  public static void main(String[] args) {

    LudoBoard board = new LudoBoard(15, 15);
    board.set(new CellPosition(0, 0), 'g');
    board.set(new CellPosition(0, 14), 'y');
    board.set(new CellPosition(14, 0), 'r');

    //tegner de gule rutene på brettet
    board.set(new CellPosition(14, 14), 'b');
    board.set(new CellPosition(14, 13), 'b');
    board.set(new CellPosition(14, 12), 'b');
    board.set(new CellPosition(14, 11), 'b');
    board.set(new CellPosition(14, 10), 'b');

    board.set(new CellPosition(14, 9), 'b');
    board.set(new CellPosition(13, 9), 'b');
    board.set(new CellPosition(12, 9), 'b');
    board.set(new CellPosition(11,9), 'b');
    board.set(new CellPosition(10, 9), 'b');
    board.set(new CellPosition(9, 9), 'b');


    board.set(new CellPosition(13, 14), 'b');
    board.set(new CellPosition(12, 14), 'b');
    board.set(new CellPosition(11, 14), 'b');
    board.set(new CellPosition(10, 14), 'b');
    board.set(new CellPosition(9, 14), 'b');

    board.set(new CellPosition(9, 10), 'b');
    board.set(new CellPosition(9, 11), 'b');
    board.set(new CellPosition(9, 12), 'b');
    board.set(new CellPosition(9, 13), 'b');

    board.set(new CellPosition(11, 11), 'b');
    board.set(new CellPosition(12, 11), 'b');
    board.set(new CellPosition(11, 12), 'b');
    board.set(new CellPosition(12, 12), 'b');

    //tegner de røde rutene på brettet
    board.set(new CellPosition(0, 0), 'r');
board.set(new CellPosition(0, 1), 'r');
board.set(new CellPosition(0, 2), 'r');
board.set(new CellPosition(0, 3), 'r');
board.set(new CellPosition(0, 4), 'r');
board.set(new CellPosition(0, 5), 'r');

board.set(new CellPosition(1, 0), 'r');

board.set(new CellPosition(1, 5), 'r');

board.set(new CellPosition(2, 0), 'r');
board.set(new CellPosition(2, 2), 'r');
board.set(new CellPosition(2, 3), 'r');
board.set(new CellPosition(2, 5), 'r');

board.set(new CellPosition(3, 0), 'r');
board.set(new CellPosition(3, 2), 'r');
board.set(new CellPosition(3, 3), 'r');
board.set(new CellPosition(3, 5), 'r');

board.set(new CellPosition(4, 0), 'r');
board.set(new CellPosition(4, 5), 'r');

board.set(new CellPosition(5, 0), 'r');
board.set(new CellPosition(5, 1), 'r');
board.set(new CellPosition(5, 2), 'r');
board.set(new CellPosition(5, 3), 'r');
board.set(new CellPosition(5, 4), 'r');
board.set(new CellPosition(5, 5), 'r');

//farger den grønne delen av brettet
board.set(new CellPosition(0, 14), 'g');
board.set(new CellPosition(0, 13), 'g');
board.set(new CellPosition(0, 12), 'g');
board.set(new CellPosition(0, 11), 'g');
board.set(new CellPosition(0, 10), 'g');
board.set(new CellPosition(0, 9), 'g');

board.set(new CellPosition(1, 14), 'g');

board.set(new CellPosition(1, 9), 'g');

board.set(new CellPosition(2, 14), 'g');

board.set(new CellPosition(2, 12), 'g');
board.set(new CellPosition(2, 11), 'g');

board.set(new CellPosition(2, 9), 'g');

board.set(new CellPosition(3, 14), 'g');

board.set(new CellPosition(3, 12), 'g');
board.set(new CellPosition(3, 11), 'g');

board.set(new CellPosition(3, 9), 'g');

board.set(new CellPosition(4, 14), 'g');

board.set(new CellPosition(4, 9), 'g');

board.set(new CellPosition(5, 14), 'g');
board.set(new CellPosition(5, 13), 'g');
board.set(new CellPosition(5, 12), 'g');
board.set(new CellPosition(5, 11), 'g');
board.set(new CellPosition(5, 10), 'g');
board.set(new CellPosition(5, 9), 'g');

//farger den blå delen av brettet
board.set(new CellPosition(14, 0), 'y');
board.set(new CellPosition(14, 1), 'y');
board.set(new CellPosition(14, 2), 'y');
board.set(new CellPosition(14, 3), 'y');
board.set(new CellPosition(14, 4), 'y');
board.set(new CellPosition(14, 5), 'y');

board.set(new CellPosition( 13, 0), 'y');
//board.set(new CellPosition(13, 1), 'y');
//board.set(new CellPosition(13, 2), 'y');
//board.set(new CellPosition(13, 3), 'y');
//board.set(new CellPosition(13, 4), 'y');
board.set(new CellPosition(13,5), 'y');

board.set(new CellPosition(12, 0), 'y');
//board.set(new CellPosition(12, 1), 'y');
board.set(new CellPosition(12, 2), 'y');
board.set(new CellPosition(12, 3), 'y');
//board.set(new CellPosition(12, 4), 'y');
board.set(new CellPosition(12, 5), 'y');

board.set(new CellPosition(11, 0), 'y');
//board.set(new CellPosition(11, 1), 'y');
board.set(new CellPosition(11, 2), 'y');
board.set(new CellPosition(11, 3), 'y');
//board.set(new CellPosition(11,4), 'y');
board.set(new CellPosition(11, 5), 'y');

board.set(new CellPosition(10, 0), 'y');
//board.set(new CellPosition(10, 1), 'y');
//board.set(new CellPosition(10, 2), 'y');
//board.set(new CellPosition(10, 3), 'y');
//board.set(new CellPosition(10, 4), 'y');
board.set(new CellPosition(10, 5), 'y');

board.set(new CellPosition(9, 0), 'y');
board.set(new CellPosition(9, 1), 'y');
board.set(new CellPosition(9, 2), 'y');
board.set(new CellPosition(9, 3), 'y');
board.set(new CellPosition(9, 4), 'y');
board.set(new CellPosition(9, 5), 'y');



//tegner de ulike veiene inn for fargene i ludo

//blå
board.set(new CellPosition(7, 8), 'b');
board.set(new CellPosition(7, 9), 'b');
board.set(new CellPosition(7, 10), 'b');
board.set(new CellPosition(7, 11), 'b');
board.set(new CellPosition(7, 12), 'b');
board.set(new CellPosition(7, 13), 'b');
board.set(new CellPosition(8, 13), 'b');
//gul
board.set(new CellPosition(8, 7), 'y');
board.set(new CellPosition(9, 7), 'y');
board.set(new CellPosition(10, 7), 'y');
board.set(new CellPosition(11, 7), 'y');
board.set(new CellPosition(12, 7), 'y');
board.set(new CellPosition(13, 7), 'y');
board.set(new CellPosition(13, 6), 'y');

//rød
board.set(new CellPosition(7, 6), 'r');
board.set(new CellPosition(7, 5), 'r');
board.set(new CellPosition(7, 4), 'r');
board.set(new CellPosition(7, 3), 'r');
board.set(new CellPosition(7, 2), 'r');
board.set(new CellPosition(7, 1), 'r');
board.set(new CellPosition(6, 1), 'r');
//grønn
board.set(new CellPosition(6, 7), 'g');
board.set(new CellPosition(5, 7), 'g');
board.set(new CellPosition(4, 7), 'g');
board.set(new CellPosition(3, 7), 'g');
board.set(new CellPosition(2, 7), 'g');
board.set(new CellPosition(1, 7), 'g');
board.set(new CellPosition(1, 8), 'g');

//fylle farge rundt midten
board.set(new CellPosition(6, 8), 'p');
board.set(new CellPosition(6, 6), 'p');
board.set(new CellPosition(8, 8), 'p');
board.set(new CellPosition(8, 6), 'p');
board.set(new CellPosition(7, 7), 'p');


//tegne brikke grønn

    
    
    
    LudoModel model = new LudoModel(board);
    LudoView view = new LudoView(model);
    new Controller(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    
    
    
    
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
